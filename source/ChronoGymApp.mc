using Toybox.Application as App;
using Toybox.WatchUi as Ui;

var elements = new[0];
var model;

class ChronoGymApp extends App.AppBase {
    function initialize() {
        AppBase.initialize();
    }

    function onStart(state) {
        extract(App.getApp().getProperty("elements"));
        // extract("[{\"name\":\"A\",\"time\":\"15\",\"intervalTime\":\"10\",\"prepTime\":\"10\",\"round\":\"3\",\"beep\":\"true\",\"beep5s\":\"true\",\"vibrate\":\"true\"}]");
    }

    function onStop(state) {
        App.getApp().setProperty("elements", elementsToJson());
    }

    function getInitialView() {
        var menu = new HomeMenu();
        return [ menu, new HomeMenuDelegate(menu) ];
    }

    function extract(json) {
        if (json != null) {
            var index = json.find("{");
            var position = 0;
            while (index != null) {
                json = json.substring(index + 1, json.length());
                var value = json.substring(0, json.find("}") + 1);
                elements.add(new Exercice(position, "{" + value));
                index = json.find("{");
                position = position + 1;
            }
        }
    }

    function elementsToJson() {
        var json = "";
        for (var i = 0; i < elements.size(); i++) {
            if (i == 0) {
                json = "[";
            }
            json = json + elements[i].toJson();
            if (i < elements.size() - 1) {
                json = json + ",";
            } else {
                json = json + "]";
            }
        }
        return json;
    }
}
