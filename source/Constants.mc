module Property {
    const BEEP = "beep";
    const BEEP_5S = "beep5s";
    const VIBRATE = "vibrate";
    const DISPLAY_HEART = "display_heart";
    const TIME_INTERVAL = "time_interval";
    const TIME_PREP = "time_prep";
    const TIME_REST = "time_rest";
    const ROUND = "round";
    const NAME = "name";
}