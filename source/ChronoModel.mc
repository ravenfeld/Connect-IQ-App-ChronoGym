using Toybox.Application as App;
using Toybox.Timer as Timer;
using Toybox.WatchUi as Ui;
using Toybox.Attention as Attention;
using Toybox.ActivityRecording as ActivityRecording;
using Toybox.Sensor as Sensor;
using Toybox.Time as Time;

class ChronoModel {
    var restTime;
    var prepTime;
    var workTime;
    var roundTotal;
    var heartRate;
    var displayHeart;

    var counter;
    var round = 0;
    var phase = Prep;
    var status;
    var session = ActivityRecording.createSession({ sport => ActivityRecording.SPORT_TRAINING, subSport => ActivityRecording.SUB_SPORT_CARDIO_TRAINING, name => Ui.loadResource(Rez.Strings.Sport)});
    var menuVisible = false;
    private
    var refreshTimer = new Timer.Timer();
    private
    var displayTimer = new Timer.Timer();
    private
    var sensors = Sensor.setEnabledSensors([Sensor.SENSOR_HEARTRATE]);
    private
    var data;

    function initialize(data) {
        self.data = data;
        restTime = data.restTime;
        workTime = data.intervalTime;
        roundTotal = data.round;
        prepTime = data.prepTime;
        displayHeart = data.displayHeart;
        if (prepTime > 0) {
            phase = Prep;
            counter = prepTime;
        } else {
            phase = :Work;
            counter = workTime;
            round = 1;
        }
        Sensor.enableSensorEvents(method( :onSensor));
    }

    function onSensor(sensorInfo) {
        heartRate = sensorInfo.heartRate;
        Ui.requestUpdate();
    }

    function startSession() {
        status = Start;
        session.start();
        refreshTimer.stop();
        displayTimer.stop();
        refreshTimer.start(method( :refresh), 1000, true);
        startBuzz();
        Ui.requestUpdate();
    }

    function stopSession(beep, displayMenu) {
        if (model.status != :Stop) {
            status = :Pause;
            if (beep) {
                stopBuzz();
            }
        }
        session.stop();
        refreshTimer.stop();
        displayTimer.stop();
        if (displayMenu) {
            displayTimer.start(method( :displayMenu), 3000, false);
            Ui.requestUpdate();
        }
    }

    function displayMenu() {
        displayTimer.stop();
        if (!menuVisible) {
            if (model.status == :Pause) {
                var menu = new PauseMenu();
                Ui.pushView(menu, new PauseEndMenuDelegate(menu), Ui.SLIDE_LEFT);
            } else {
                var menu = new EndMenu();
                Ui.pushView(menu, new PauseEndMenuDelegate(menu), Ui.SLIDE_LEFT);
            }
        }
    }

    function resetSession() {
        status = null;
        if (prepTime > 0) {
            phase = :Prep;
            counter = prepTime;
            round = 0;
        } else {
            phase = :Work;
            counter = workTime;
            round = 1;
        }
    }

    function refresh() {
        status = :Work;
        if (counter > 1) {
            counter--;
            if (counter <= 5) {
                buzz();
            }
        } else {
            if (phase == :Prep) {
                phase = :Work;
                counter = workTime;
                round++;
                lap();
            } else if (phase == :Work && restTime > 0) {
                phase = :Rest;
                counter = restTime;
                lap();
            } else if (phase == :Rest || (phase == :Work && restTime == 0)) {
                if (round == roundTotal) {
                    status = :Stop;
                    counter = 0;
                    end();
                } else {
                    phase = :Work;
                    counter = workTime;
                    round++;
                    lap();
                }
            }
        }
        Ui.requestUpdate();
    }

    function end() {
        stopSession(false, true);
        endBuzz();
    }

    function lap() {
        session.addLap();
        intervalBuzz();
    }

    function saveSession() {
        Sensor.enableSensorEvents(null);
        refreshTimer.stop();
        session.stop();
        session.save();
    }

    function startBuzz() {
        if (data.beep && Attention has :playTone) {
            beep(Attention.TONE_START);
        }
        vibrate(1500);
    }

    function stopBuzz() {
        if (data.beep && Attention has :playTone) {
            beep(Attention.TONE_STOP);
        }
        vibrate(1500);
    }

    function endBuzz() {
        if (data.beep && Attention has :playTone) {
            beep(Attention.TONE_SUCCESS);
        }
        vibrate(1500);
    }

    function buzz() {
        if (data.beep && data.beep5s && Attention has :playTone) {
            beep(Attention.TONE_LOUD_BEEP);
        }
    }

    function intervalBuzz() {
        if (data.beep && Attention has :playTone) {
            beep(Attention.TONE_INTERVAL_ALERT);
        }
        vibrate(1000);
    }

    function vibrate(duration) {
        if (data.vibrate && Attention has :vibrate) {
            var vibrateData = [new Attention.VibeProfile(100, duration)];
            Attention.vibrate(vibrateData);
        }
    }

    function beep(tone) {
        if (Attention has :playTone) {
            Attention.playTone(tone);
        }
    }

    function dropSession() {
        Sensor.enableSensorEvents(null);
        refreshTimer.stop();
        displayTimer.stop();
        session.stop();
        session.discard();
    }
}
