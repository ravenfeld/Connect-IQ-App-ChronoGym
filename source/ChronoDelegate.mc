using Toybox.WatchUi as Ui;

class ChronoDelegate extends Ui.BehaviorDelegate {
    private
    var bip;
    private
    var bip5S;
    private
    var vibrate;
    private
    var data;

    function initialize(data) {
        BehaviorDelegate.initialize();
        self.data = data;
        bip = Ui.loadResource(Rez.Strings.Bip);
        bip5S = Ui.loadResource(Rez.Strings.Bip5s);
        vibrate = Ui.loadResource(Rez.Strings.Vibrate);
    }

    function onSelect() {
        if (model.status == null || model.status == :Pause) {
            model.startSession();
        } else {
            model.stopSession(true, true);
        }
    }

    function onMenu() {
        var menu = new EditMenu(false);
        Ui.pushView(menu, new EditMenuDelegate(menu), Ui.SLIDE_RIGHT);
    }

    function onBack() {
        if (model.status == null) {
            model.dropSession();
            Ui.popView(Ui.SLIDE_IMMEDIATE);
        } else if (model.status != :Work) {
            model.displayMenu();
        }
        return true;
    }
}
