class Exercice {
    var position;
    var name;
    var restTime;
    var intervalTime;
    var prepTime;
    var round;
    var beep;
    var beep5s;
    var vibrate;
    var displayHeart;

    function initialize(position, json) {
        self.position = position;
        self.name = extractValue(Property.NAME, json);
        var intervalTimeString = extractValue(Property.TIME_INTERVAL, json);
        if (intervalTimeString.equals("")) {
            self.intervalTime = 60;
        } else {
            self.intervalTime = intervalTimeString.toNumber();
        }
        var restTimeString = extractValue(Property.TIME_REST, json);
        if (restTimeString.equals("")) {
            self.restTime = 60;
        } else {
            self.restTime = restTimeString.toNumber();
        }
        var prepTimeString = extractValue(Property.TIME_PREP, json);
        if (prepTimeString.equals("")) {
            self.prepTime = 5;
        } else {
            self.prepTime = prepTimeString.toNumber();
        }
        var roundString = extractValue(Property.ROUND, json);
        if (roundString.equals("")) {
            self.round = 2;
        } else {
            self.round = roundString.toNumber();
        }
        self.beep = !extractValue(Property.BEEP, json).equals("false") ? true : false;
        self.beep5s = !extractValue(Property.BEEP_5S, json).equals("false") ? true : false;
        self.vibrate = !extractValue(Property.VIBRATE, json).equals("false") ? true : false;
        self.displayHeart = extractValue(Property.DISPLAY_HEART, json).equals("true") ? true : false;
    }

    function setPosition(position) {
        self.position = position;
    }

    function extractValue(key, json) {
        var index = json.find("\"" + key + "\":");
        if (index != null) {
            var value = json.substring(index, json.length());
            var indexEnd = value.find("\",");
            if (indexEnd == null) {
                indexEnd = value.length() - 2;
            }
            value = value.substring(value.find(":\"") + 2, indexEnd);
            return value;
        }
        return "";
    }

    function getProperty(property) {
        if (property.equals(Property.NAME)) {
            return name;
        } else if (property.equals(Property.TIME_REST)) {
            return restTime;
        } else if (property.equals(Property.TIME_INTERVAL)) {
            return intervalTime;
        } else if (property.equals(Property.TIME_PREP)) {
            return prepTime;
        } else if (property.equals(Property.ROUND)) {
            return round;
        } else if (property.equals(Property.BEEP)) {
            return beep;
        } else if (property.equals(Property.BEEP_5S)) {
            return beep5s;
        } else if (property.equals(Property.VIBRATE)) {
            return vibrate;
        } else if (property.equals(Property.DISPLAY_HEART)) {
            return displayHeart;
        } else {
            return null;
        }
    }

    function setProperty(property, value) {
        if (property.equals(Property.NAME)) {
            name = value;
        } else if (property.equals(Property.TIME_REST)) {
            restTime = value;
        } else if (property.equals(Property.TIME_INTERVAL)) {
            intervalTime = value;
        } else if (property.equals(Property.TIME_PREP)) {
            prepTime = value;
        } else if (property.equals(Property.ROUND)) {
            round = value;
        } else if (property.equals(Property.BEEP)) {
            beep = value;
        } else if (property.equals(Property.BEEP_5S)) {
            beep5s = value;
        } else if (property.equals(Property.VIBRATE)) {
            vibrate = value;
        } else if (property.equals(Property.DISPLAY_HEART)) {
            displayHeart = value;
        }
    }

    function toJson() {
        return "{\"" + Property.NAME + "\":\"" + name + "\",\"" + Property.TIME_REST + "\":\"" + restTime + "\",\"" + Property.TIME_INTERVAL + "\":\"" + intervalTime + "\",\"" + Property.TIME_PREP + "\":\"" + prepTime + "\",\"" + Property.ROUND + "\":\"" + round + "\",\"" + Property.BEEP + "\":\"" + beep + "\",\"" + Property.BEEP_5S + "\":\"" + beep5s + "\",\"" + Property.VIBRATE + "\":\"" +
               vibrate + "\",\"" + Property.DISPLAY_HEART + "\":\"" + displayHeart + "\"}";
    }
}