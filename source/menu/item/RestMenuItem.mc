using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class RestMenuItem extends DataMenuItem {
    function initialize(id, data) {
        DataMenuItem.initialize(id, Ui.loadResource(Rez.Strings.Rest), data);
    }

    function onShow() {
        MenuItem.setValue(Utils.timeToString(data.restTime));
    }
}