using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class ExerciceMenuItem extends DataMenuItem {
    private
    var itemSubLabelRestString;
    private
    var itemSubLabelString;

    function initialize(id, data) {
        System.println(data.toJson());
        itemSubLabelRestString = Ui.loadResource(Rez.Strings.ItemSubLabelRest);
        itemSubLabelString = Ui.loadResource(Rez.Strings.ItemSubLabel);
        DataMenuItem.initialize(id, data.name, data);
    }

    function onShow() {
        var round = data.round;
        var interval = Utils.timeToString(data.intervalTime);
        var restValue = data.restTime;
        if (restValue > 0) {
            var rest = Utils.timeToString(restValue);
            DataMenuItem.setValue(Lang.format(itemSubLabelRestString, [ round, interval, rest ]));
        } else {
            DataMenuItem.setValue(Lang.format(itemSubLabelString, [ round, interval ]));
        }
    }
}