using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class RoundMenuItem extends DataMenuItem {
    function initialize(id, data) {
        DataMenuItem.initialize(id, Ui.loadResource(Rez.Strings.Repeat), data);
    }

    function onShow() {
        MenuItem.setValue(data.round + "x");
    }
}