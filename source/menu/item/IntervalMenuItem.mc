using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class IntervalMenuItem extends DataMenuItem {
    function initialize(id, data) {
        DataMenuItem.initialize(id, Ui.loadResource(Rez.Strings.Interval), data);
    }

    function onShow() {
        MenuItem.setValue(Utils.timeToString(data.intervalTime));
    }
}