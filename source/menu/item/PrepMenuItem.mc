using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class PrepMenuItem extends DataMenuItem {
    function initialize(id, data) {
        DataMenuItem.initialize(id, Ui.loadResource(Rez.Strings.Preparation), data);
    }

    function onShow() {
        MenuItem.setValue(Utils.timeToString(data.prepTime));
    }
}