using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Application as App;

class EditMenu extends Menu {
    function initialize(full, data) {
        var menuItems = new[0];
        if (full) {
            menuItems.add(new PrepMenuItem( :Prep, data));
            menuItems.add(new IntervalMenuItem( :Interval, data));
            menuItems.add(new RestMenuItem( :Rest, data));
            menuItems.add(new RoundMenuItem( :Repeat, data));
        }
        menuItems.add(new DataMenuItem( :Alarms, Ui.loadResource(Rez.Strings.Alarms), data));
        menuItems.add(new StateMenuItem( :DisplayHeart, Ui.loadResource(Rez.Strings.DisplayHeart), data, Property.DISPLAY_HEART));
        if (full) {
            menuItems.add(new DataMenuItem( :Rename, Ui.loadResource(Rez.Strings.Rename), data));
        }
        Menu.initialize(menuItems, Ui.loadResource(Rez.Strings.Edit));
    }
}

class EditMenuDelegate extends MenuDelegate {
    private
    var intervalString;
    private
    var restString;
    private
    var repeatString;

    function initialize(menu) {
        MenuDelegate.initialize(menu);
        intervalString = Ui.loadResource(Rez.Strings.EditInterval);
        restString = Ui.loadResource(Rez.Strings.EditRest);
        repeatString = Ui.loadResource(Rez.Strings.EditRepeat);
        if (model != null) {
            model.menuVisible = true;
        }
    }

    function onMenuItem(item) {
        if (item.id == :Prep) {
            Ui.pushView(new TimePicker(Ui.loadResource(Rez.Strings.EditPrep), item.data, Property.TIME_PREP), new TimePickerDelegate(item.data, Property.TIME_PREP), Ui.SLIDE_IMMEDIATE);
        } else if (item.id == :Interval) {
            Ui.pushView(new TimePicker(intervalString, item.data, Property.TIME_INTERVAL), new TimePickerDelegate(item.data, Property.TIME_INTERVAL), Ui.SLIDE_IMMEDIATE);
        } else if (item.id == :Rest) {
            Ui.pushView(new TimePicker(restString, item.data, Property.TIME_REST), new TimePickerDelegate(item.data, Property.TIME_REST), Ui.SLIDE_IMMEDIATE);
        } else if (item.id == :Repeat) {
            Ui.pushView(new NumberPicker(repeatString, item.data, Property.ROUND), new NumberPickerDelegate(item.data, Property.ROUND), Ui.SLIDE_IMMEDIATE);
        } else if (item.id == :Alarms) {
            var menu = new AlarmMenu(item.data);
            Ui.pushView(menu, new AlarmMenuDelegate(menu), Ui.SLIDE_LEFT);
        } else if (item.id == :DisplayHeart) {
            item.data.displayHeart = !item.data.displayHeart;
        } else if (item.id == :Rename) {
            if (Ui has :TextPicker) {
                Ui.pushView(new Ui.TextPicker(item.data.name), new EditExercicePicker(item.data), Ui.SLIDE_LEFT);
            }
        }
    }

    function onBack() {
        if (model != null) {
            model.menuVisible = false;
        }
    }
}