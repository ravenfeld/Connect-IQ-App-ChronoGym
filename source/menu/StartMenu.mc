using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Application as App;

class StartMenu extends Menu {
    private
    var data;

    function initialize(data) {
        self.data = data;
        var menuItems = new[0];
        menuItems.add(new DataMenuItem( :Start, Ui.loadResource(Rez.Strings.Start), data));
        menuItems.add(new DataMenuItem( :Edit, Ui.loadResource(Rez.Strings.Edit), data));
        if ( elements.size() > 1 ) {
            menuItems.add(new DataMenuItem( :Up, Ui.loadResource(Rez.Strings.Up), data));
            menuItems.add(new DataMenuItem( :Down, Ui.loadResource(Rez.Strings.Down), data));
        }
        menuItems.add(new DataMenuItem( :Remove, Ui.loadResource(Rez.Strings.Remove), data));
        Menu.initialize(menuItems, "");
    }

    function onShow() {
        var round = data.round;
        var interval = Utils.timeToString(data.intervalTime);
        var restValue = data.restTime;
        if (restValue > 0) {
            var rest = Utils.timeToString(restValue);
            Menu.setTitle(Lang.format(Ui.loadResource(Rez.Strings.StartTitleRest), [ round, interval, rest ]));
        } else {
            Menu.setTitle(Lang.format(Ui.loadResource(Rez.Strings.StartTitle), [ round, interval ]));
        }

        Menu.onShow();
    }
}

class StartMenuDelegate extends MenuDelegate {
    function initialize(menu) {
        MenuDelegate.initialize(menu);
    }

    function onMenuItem(item) {
        if (item.id == :Start) {
            model = new ChronoModel(item.data);
            Ui.pushView(new ChronoView(), new ChronoDelegate(item.data), Ui.SLIDE_IMMEDIATE);
        } else if (item.id == :Edit) {
            var menu = new EditMenu(true, item.data);

            Ui.pushView(menu, new EditMenuDelegate(menu), Ui.SLIDE_LEFT);
        } else if (item.id == :Up) {
            var position = item.data.position;
            if (position > 0) {
                var temps = new[0];
                for (var i = 0; i < elements.size(); i++) {
                    if (i == position - 1) {
                        item.data.setPosition(temps.size());
                        temps.add(item.data);
                    }
                    if (i != position) {
                        elements[i].setPosition(temps.size());
                        temps.add(elements[i]);
                    }
                }
                elements = new[0];
                elements.addAll(temps);
            }
            Ui.popView(Ui.SLIDE_LEFT);
        } else if (item.id == :Down) {
            var position = item.data.position;
            if (position < elements.size() - 1) {
                var temps = new[0];
                for (var i = 0; i < elements.size(); i++) {
                    if (i != position) {
                        elements[i].setPosition(temps.size());
                        temps.add(elements[i]);
                    }
                    if (i == position + 1) {
                        item.data.setPosition(temps.size());
                        temps.add(item.data);
                    }
                }
                elements = new[0];
                elements.addAll(temps);
            }
            Ui.popView(Ui.SLIDE_LEFT);
        } else if (item.id == :Remove) {
            elements.remove(item.data);
            Ui.popView(Ui.SLIDE_LEFT);
        }
    }
}