using Toybox.WatchUi as Ui;
using Toybox.System as Sys;

class EndMenu extends Menu {
    function initialize() {
        var menuItems = new[0];
        menuItems.add(new MenuItem( :Save, Ui.loadResource(Rez.Strings.Save)));
        menuItems.add(new MenuItem( :Restart, Ui.loadResource(Rez.Strings.Restart)));
        menuItems.add(new MenuItem( :IgnoreEnd, Ui.loadResource(Rez.Strings.Ignore)));
        menuItems.add(new MenuItem( :Home, Ui.loadResource(Rez.Strings.Home)));
        Menu.initialize(menuItems, Ui.loadResource(Rez.Strings.EndMenuTitle));
    }
}

class PauseMenu extends Menu {
    function initialize() {
        var menuItems = new[0];
        menuItems.add(new MenuItem( :Continue, Ui.loadResource(Rez.Strings.Continue)));
        menuItems.add(new MenuItem( :Save, Ui.loadResource(Rez.Strings.Save)));
        menuItems.add(new MenuItem( :Restart, Ui.loadResource(Rez.Strings.Restart)));
        menuItems.add(new MenuItem( :IgnorePause, Ui.loadResource(Rez.Strings.Ignore)));
        Menu.initialize(menuItems, Ui.loadResource(Rez.Strings.PauseMenuTitle));
    }
}

class PauseEndMenuDelegate extends MenuDelegate {
    function initialize(menu) {
        MenuDelegate.initialize(menu);
    }

    function onMenuItem(item) {
        if (item.id == :Continue) {
            model.startSession();
            Ui.popView(Ui.SLIDE_IMMEDIATE);
        } else if (item.id == :Save) {
            model.saveSession();
            Toybox.System.exit();
        } else if (item.id == :IgnoreEnd) {
            var menu = new ConfirmationMenu(Ui.loadResource(Rez.Strings.ConfirmIgnore));
            Ui.pushView(menu, new ConfirmationMenuDelegate(menu, method( :onConfirmYesEnd), method( :onConfirmNo)), Ui.SLIDE_IMMEDIATE);
        } else if (item.id == :IgnorePause) {
            var menu = new ConfirmationMenu(Ui.loadResource(Rez.Strings.ConfirmIgnore));
            Ui.pushView(menu, new ConfirmationMenuDelegate(menu, method( :onConfirmYesPause), method( :onConfirmNo)), Ui.SLIDE_IMMEDIATE);
        } else if (item.id == :Restart) {
            model.resetSession();
            Ui.popView(Ui.SLIDE_IMMEDIATE);
        } else if (item.id == :Home) {
            Ui.popView(Ui.SLIDE_IMMEDIATE);
            Ui.popView(Ui.SLIDE_IMMEDIATE);
            Ui.popView(Ui.SLIDE_IMMEDIATE);
        }
    }

    function onConfirmYesEnd() {
        model.dropSession();
        Toybox.System.exit();
    }

    function onConfirmYesPause() {
        model.dropSession();
        Ui.popView(Ui.SLIDE_IMMEDIATE);
        Ui.popView(Ui.SLIDE_IMMEDIATE);
        Ui.popView(Ui.SLIDE_IMMEDIATE);
    }

    function onConfirmNo() {
        Ui.popView(Ui.SLIDE_IMMEDIATE);
    }
}
