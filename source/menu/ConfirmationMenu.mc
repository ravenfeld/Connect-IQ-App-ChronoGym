using Toybox.WatchUi as Ui;

class ConfirmationMenu extends Menu {
    function initialize(title) {
        var menuItems = new[0];
        menuItems.add(new MenuItem( :No, Ui.loadResource(Rez.Strings.No)));
        menuItems.add(new MenuItem( :Yes, Ui.loadResource(Rez.Strings.Yes)));
        Menu.initialize(menuItems, title);
    }
}

class ConfirmationMenuDelegate extends MenuDelegate {
    private
    var callbackYes;
    private
    var callbackNo;

    function initialize(menu, onYes, onNo) {
        self.callbackYes = onYes;
        self.callbackNo = onNo;
        MenuDelegate.initialize(menu);
    }

    function onMenuItem(item) {
        if (item.id == :Yes) {
            callbackYes.invoke();
        } else if (item.id == :No) {
            callbackNo.invoke();
        }
    }
}