using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class HomeMenu extends Menu {
    private
    var addString;

    function initialize() {
        Menu.initialize(null, null);
        addString = Ui.loadResource(Rez.Strings.Add);
    }

    function onShow() {
        var menuArray = new[elements.size() + 1];

        for (var i = 0; i < elements.size(); i++) {
            menuArray[i] = new ExerciceMenuItem(i, elements[i]);
        }

        menuArray[elements.size()] = new MenuItem( :Add, addString);

        Menu.setMenuArray(menuArray);
        Menu.onShow();
    }
}

class HomeMenuDelegate extends MenuDelegate {
    function initialize(menu) {
        MenuDelegate.initialize(menu);
    }

    function onMenuItem(item) {
        if (item.id == :Add) {
            if (Ui has :TextPicker) {
                Ui.pushView(new Ui.TextPicker(""), new AddExercicePicker(), Ui.SLIDE_LEFT);
            }
        } else {
            var menu = new StartMenu(item.data);
            Ui.pushView(menu, new StartMenuDelegate(menu), Ui.SLIDE_LEFT);
        }
    }
}