using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Application as App;

class AlarmMenu extends Menu {
    function initialize(data) {
        var menuItems = new[0];

        if (Attention has :playTone) {
            menuItems.add(new StateMenuItem( :Beep, Ui.loadResource(Rez.Strings.Bip), data, Property.BEEP));
            menuItems.add(new StateMenuItem( :Beep5s, Ui.loadResource(Rez.Strings.Bip5s), data, Property.BEEP_5S));
        }
        if (Attention has :vibrate) {
            menuItems.add(new StateMenuItem( :Vibrate, Ui.loadResource(Rez.Strings.Vibrate), data, Property.VIBRATE));
        }
        Menu.initialize(menuItems, Ui.loadResource(Rez.Strings.Alarms));
    }

}

class AlarmMenuDelegate extends MenuDelegate {
    function initialize(menu) {
        MenuDelegate.initialize(menu);
    }

    function onMenuItem(item) {
        if (item.id == :Beep) {
            item.data.beep = !item.data.beep;
            if (item.data.beep == false ) {
                item.data.beep5s = false;
            }
        } else if (item.id == :Beep5s) {
            if (item.data.beep == true) {
                item.data.beep5s = !item.data.beep5s;
            }
        } else if (item.id == :Vibrate) {
            item.data.vibrate = !item.data.vibrate;
        }
    }
}