using Toybox.WatchUi as Ui;

class EditExercicePicker extends Ui.TextPickerDelegate {
    private
    var data;

    function initialize(data) {
        self.data = data;
        Ui.TextPickerDelegate.initialize();
    }

    function onTextEntered(text, changed) {
        data.name = text;
    }

    function onCancel() {
        // Nothing
    }
}