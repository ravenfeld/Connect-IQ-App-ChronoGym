using Toybox.WatchUi as Ui;

class AddExercicePicker extends Ui.TextPickerDelegate {
    function initialize() {
        Ui.TextPickerDelegate.initialize();
    }

    function onTextEntered(text, changed) {
        elements.add(new Exercice(elements.size(), "{\"" + Property.NAME + "\":\"" + text + "\"}"));
    }

    function onCancel() {
        // Nothing
    }
}